/**
 * @file /include/gs_gui/qnode.hpp
 *
 * @brief Communications central!
 *
 * @date February 2011
 **/
/*****************************************************************************
** Ifdefs
*****************************************************************************/

#ifndef gs_gui_QNODE_HPP_
#define gs_gui_QNODE_HPP_

/*****************************************************************************
** Includes
*****************************************************************************/

#include <ros/ros.h>
#include <string>
#include <QThread>
#include <QStringListModel>
#include <ardrone_autonomy/Ranges.h>
#include <ardrone_autonomy/Navdata.h>
#include <ardrone_autonomy/navdata_magneto.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/Image.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Twist.h>



/*****************************************************************************
** Namespaces
*****************************************************************************/

namespace gs_gui {

/*****************************************************************************
** Class
*****************************************************************************/

class QNode: public QThread {
    Q_OBJECT;
public:
	QNode(int argc, char** argv );
	virtual ~QNode();
	bool init();
    void run();
    void sendStartMsg(void);
    void sendLandMsg(void);
    void sendResetMsg(void);
    void sendAutoControlMsg(void);
    void sendManualControl(geometry_msgs::Twist msg);
    void cbRanges(ardrone_autonomy::Ranges msg);
    void cbNavi(ardrone_autonomy::Navdata msg);
    void cbNaviMag(ardrone_autonomy::navdata_magneto msg);
    void cbCamera(const sensor_msgs::ImageConstPtr& msg);
    void cbPose(geometry_msgs::PoseStamped msg);

Q_SIGNALS:
    void rangesChanged(double, double, double);
    void navDataChanged(ardrone_autonomy::Navdata);
    void navDataMagChanged(ardrone_autonomy::navdata_magneto);
    void cameraImageChanged(const sensor_msgs::ImageConstPtr &);
    void poseChanged(geometry_msgs::PoseStamped);

private:
	int init_argc;
	char** init_argv;

    int countNavMsg;
    int countNavMagMsg;
    int countPoseMsg;

    ros::Publisher start_publisher;
    ros::Publisher land_publisher;
    ros::Publisher reset_publisher;
    ros::Publisher manualControl_publisher;
    ros::Publisher autoControl_publisher;
    ros::Subscriber ranges_sub; // USB Data
    ros::Subscriber navi_sub; //NAVDATA
    ros::Subscriber navi_mag_sub; //Navdata magneto
    ros::Subscriber pose_sub; //position
    image_transport::Subscriber camera_sub; //Ar.Drone camera
};

}  // namespace gs_gui

#endif /* gs_gui_QNODE_HPP_ */

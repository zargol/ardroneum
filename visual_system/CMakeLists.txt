# Catkin User Guide: http://www.ros.org/doc/groovy/api/catkin/html/user_guide/user_guide.html
# Catkin CMake Standard: http://www.ros.org/doc/groovy/api/catkin/html/user_guide/standards.html
cmake_minimum_required(VERSION 2.8.3)
project(visual_system)
# Load catkin and all dependencies required for this package
# TODO: remove all from COMPONENTS that are not catkin packages.
find_package(catkin REQUIRED COMPONENTS message_generation  ardrone_autonomy std_msgs roscpp sensor_msgs cv_bridge roscpp std_msgs image_transport)

# include_directories(include ${Boost_INCLUDE_DIR} ${catkin_INCLUDE_DIRS})
# CATKIN_MIGRATION: removed during catkin migration
# cmake_minimum_required(VERSION 2.4.6)

# CATKIN_MIGRATION: removed during catkin migration
# include($ENV{ROS_ROOT}/core/rosbuild/rosbuild.cmake)

# Set the build type.  Options are:
#  Coverage       : w/ debug symbols, w/o optimization, w/ code-coverage
#  Debug          : w/ debug symbols, w/o optimization
#  Release        : w/o debug symbols, w/ optimization
#  RelWithDebInfo : w/ debug symbols, w/ optimization
#  MinSizeRel     : w/o debug symbols, w/ optimization, stripped binaries
#set(ROS_BUILD_TYPE RelWithDebInfo)


# CATKIN_MIGRATION: removed during catkin migration
# rosbuild_init()

#set the default path for built executables to the "bin" directory
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)
#set the default path for built libraries to the "lib" directory
set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/lib)

#uncomment if you have defined messages
add_message_files(
  FILES
    Cross.msg
    Dashline.msg
    DebugDashLines.msg
)
#uncomment if you have defined services
#add_service_files(
  # FILES
  # TODO: List your msg files here
#)

#common commands for building c++ executables and libraries
#add_library(${PROJECT_NAME} src/example.cpp)
#target_link_libraries(${PROJECT_NAME} another_library)
#
# CATKIN_MIGRATION: removed during catkin migration
# rosbuild_add_boost_directories()
#find_package(Boost REQUIRED COMPONENTS thread)
include_directories(${Boost_INCLUDE_DIRS})
#target_link_libraries(${PROJECT_NAME} ${Boost_LIBRARIES})

#target_link_libraries(example ${PROJECT_NAME})

add_executable(tag_detection src/tag_detection.cpp)
add_dependencies(tag_detection visual_system_generate_messages_cpp)
target_link_libraries(tag_detection ${Boost_LIBRARIES} ${catkin_LIBRARIES})

add_executable(dashline_detection src/dashline_detection.cpp)
add_dependencies(dashline_detection visual_system_generate_messages_cpp)
target_link_libraries(dashline_detection ${Boost_LIBRARIES} ${catkin_LIBRARIES})


## Generate added messages and services with any dependencies listed here
generate_messages(
    DEPENDENCIES std_msgs
)


# catkin_package parameters: http://ros.org/doc/groovy/api/catkin/html/dev_guide/generated_cmake_api.html#catkin-package
# TODO: fill in what other packages will need to use this package
catkin_package(
    DEPENDS std_msgs ardrone_autonomy roscpp sensor_msgs opencv2 cv_bridge roscpp std_msgs image_transport
    CATKIN_DEPENDS message_runtime# TODO
    INCLUDE_DIRS # TODO include
    LIBRARIES # TODO
)

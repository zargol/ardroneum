/*
 * PathFollower.h
 *
 *  Created on: Nov 27, 2013
 *      Author: alex
 */

#ifndef PATHFOLLOWER_H_
#define PATHFOLLOWER_H_

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <tf/tf.h>


class PathFollower {
public:
	PathFollower(ros::Publisher *pub);
	virtual ~PathFollower();
	bool calculateVelocityCommand(tf::Pose &_robot_pose, geometry_msgs::Twist& _cmd_vel);
	void setPlan(const std::vector<tf::Pose>& _plan);
	void generateEightPath();
    void generateSimplePath();
private:
	double K_trans_, K_rot_, tolerance_trans_, tolerance_rot_;
	unsigned int current_waypoint_;
    int countMsg;
	std::vector<tf::Pose> plan_;
	ros::Publisher *pub_pose_;
	geometry_msgs::Twist diff2D(const tf::Pose& pose1, const tf::Pose& pose2);
	geometry_msgs::Twist limitTwist(const geometry_msgs::Twist& twist);
};

#endif /* PATHFOLLOWER_H_ */

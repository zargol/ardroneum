#!/usr/bin/env python

import math
import os
import roslib
roslib.load_manifest('particles_filter')
import rospy
import std_msgs.msg

from geometry_msgs.msg import Twist
from geometry_msgs.msg import Point32
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseArray
from sensor_msgs.msg import Range
from ardrone_autonomy.msg import Ranges, Navdata

import particlesfilter

PARTICLES_COUNT = 100  # Total number of particles
SENSORS_COUNT = 3  # Total number of sensors per particle
SENSORS_ANGLES = [0, math.pi/2, 3*math.pi/2]
MAX_SENSORS_RANGE = 15.00  # Proximity sensor's max range

WIDTH = 15  # Real-map width
HEIGHT = 4  # Real-map height
SCALE = 10  # Map scale (map_coords/scale = real_coords)

# from MapData import MAP_DATA
from pam2map import pam2map
MAP_DATA = pam2map(os.path.dirname(__file__) +
                   os.sep + ".." + os.sep + "map150x40.pgm")
NORD = math.pi / 2

PROX_SENS_UPDATE_TIME = 0.05
UPDATE_TIME_EXP_RATIO = 2


#------------------------------------------------------------------------------
class Node:
    """Particles filter ROS node
    Provides interactions mechanisms between proximity/velocities sensors
    topics, resulting particles cloud publisher and Particles Filter
    implementation class.
    """
    def __init__(self):
        rospy.Subscriber('/ardrone/ranges',
                         Ranges, self.sub_prox_sens_new)

        rospy.Subscriber('/ardrone/navdata',
                         Navdata, self.sub_nav)

        self.initialized = False


        self.last_time = 0.0
        self.current_time = 0.0
        self.update_time = [0] * SENSORS_COUNT
        self.FLY = False #flag is true if ar.drone is flying 


        self.pub_particles_cloud = rospy.Publisher('particle_cloud', PoseArray)

        self.particles_filter = particlesfilter.ParticlesFilter(
            PARTICLES_COUNT, SENSORS_COUNT, WIDTH, HEIGHT, MAP_DATA, SCALE)


    def sub_prox_sens_new(self, msg):
        """Proximity sensor message handler"""
        if self.FLY:
	  ranges = [msg.range_front/100, msg.range_left/100,  msg.range_right/100]
	  
	  for i in range(SENSORS_COUNT):
	    self.update_time[i] = self.current_time
	    if ranges[i]>0:
	      self.particles_filter.update_by_sensor(i, SENSORS_ANGLES[i],
						  ranges[i],
						  MAX_SENSORS_RANGE)
	  self.pub_particles()

    def sub_nav(self, msg):
        """ """
        if msg.state == 3 or msg.state == 4 or msg.state == 6  or msg.state == 7:
	  self.FLY = True
	else:
	  self.FLY = False
	  
	if self.FLY:
	  dtime = self.current_time - self.last_time
	  self.particles_filter.particles_set_heading(0.0) #NORD + msg.rotZ * math.pi / 180)
	  self.particles_filter.particles_advance(msg.vx/1000, msg.vy/1000, dtime)
	  self.current_time = msg.header.stamp.secs
	  self.last_time = self.current_time

    def pub_particles(self):
        ps = PoseArray()
        ps.header.frame_id = "/map"
        ps.header.stamp = rospy.Time(self.current_time)
        for p in self.particles_filter.particles:
            pose = Pose()
            pose.position.x = p.x
            pose.position.y = p.y
            pose.position.z = 0
            pose.orientation.x = 0
            pose.orientation.y = 0
            pose.orientation.z = p.h
            pose.orientation.w = 1
            ps.poses.append(pose)
        # pose = Pose()
        # pose.position.x = self.particles_filter.center.x
        # pose.position.y = HEIGHT - self.particles_filter.center.y
        # pose.position.z = 0
        # pose.orientation.x = 0
        # pose.orientation.y = 0
        # pose.orientation.z = self.particles_filter.center.h
        # pose.orientation.w = 1
        # ps.poses.append(pose)
        self.pub_particles_cloud.publish(ps)


#------------------------------------------------------------------------------
def main():
    rospy.init_node('particles_filter_node')
    node = Node()
    rospy.spin()

if __name__ == '__main__':
    main()

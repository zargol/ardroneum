import bisect
import math
import random
import rospy

from ObstacleMap import ObstacleMap

#------------------------------------------------------------------------------
# Some utility functions


def add_noise(level, *coords):
    return [x + random.uniform(-level, level) for x in coords]


# This is just a gaussian kernel I pulled out of my hat, to transform
# values near to robbie's measurement => 1, further away => 0
def w_gauss(a, b):
    sigma2 = 0.9 ** 2
    error = a - b
    g = math.e ** -(error**2 / (2*sigma2))
    return g


def random_free_place(minX, maxX, minY, maxY, angle=(0.0)):
    x = random.uniform(minX, maxX)
    y = random.uniform(minY, maxY)
    h = random.uniform(0, angle)
    return x, y, h


def get_ang_xy(x, y):
    ang = 0
    if x != 0:
        ang = math.atan(math.fabs(y / x))
        if x < 0:
            ang = math.pi - ang
    else:
        ang = math.pi / 2
    if y < 0:
        ang = -1 * ang
    return ang


#------------------------------------------------------------------------------
class Point(object):
    """Utility point class"""
    def __init__(self, x=0, y=0, h=0):
        self.x = x
        self.y = y
        self.h = h

#------------------------------------------------------------------------------
class Particle(object):
    """Particles Filter particle-state class"""
    def __init__(self, x, y, heading=0, sensors_count=1, w=0, weights=None,
                 noisy=False):
        if not heading:
            #random.uniform(0, 360)
            heading = 0
        if noisy:
            x, y, heading = add_noise(0.1, x, y, heading)

        self.x = x
        self.y = y
        self.h = heading
        self.w = w
        if not weights:
            self.weights = [w/sensors_count for _ in range(sensors_count)]
        else:
            self.weights = weights[:]

    def __str__(self):
        return "(%f, %f, %f, w=%f)" % (self.x, self.y, self.h, self.w)

    @property
    def xyh(self):
        return self.x, self.y, self.h

    @classmethod
    def create_random(cls, count, minX, maxX, minY, maxY, sens_count):
        return [cls(*random_free_place(minX, maxX, minY, maxY),
                sensors_count=sens_count) for _ in range(0, count)]

    @classmethod
    def copy(cls, particle, noisy=False):
        return cls(particle.x, particle.y, particle.h,
                   len(particle.weights), w=particle.w, 
                   weights=particle.weights, noisy=noisy)

    def advance_by(self, speed, step, ang_xy, noisy=False):
        h = self.h
        if noisy:
            speed, h = add_noise(0.02, speed, h)
            # needs more noise to disperse better
            # h += random.uniform(-3, 3)
        r = h + ang_xy

        dx = math.cos(r) * speed*step
        dy = math.sin(r) * speed*step
        self.move_by(dx, dy)

    def move_by(self, dx, dy):
        self.x += dx
        self.y += dy


#------------------------------------------------------------------------------
class WeightedDistribution(object):
    """Provide picking mechanism for weighted deistribution"""
    def __init__(self, state):
        accum = 0.0
        self.state = [Particle.copy(p) for p in state if p.w > 0]
        self.distribution = []
        for x in self.state:
            accum += x.w
            self.distribution.append(accum)

    def pick(self):
        # roulette wheel selection
        index = bisect.bisect_left(self.distribution, random.uniform(0, 1))
        #print ("Index of pick = %d" % (index))
        try:
            return self.state[index]
        except IndexError:
            print("IndErr, index = %d, len = %d" % (index, len(self.state)))
            # Happens when all particles are improbable w=0
            return None


#------------------------------------------------------------------------------
class ParticlesFilter(object):
    """Particles Filter's state class"""
    def __init__(self, particles_count, sensors_count, map_width, map_height,
                 map_data, map_scale, elite_ratio=0.01):
        self.particles_count = particles_count
        self.elite_count = int(particles_count * elite_ratio)
        self.sensors_count = sensors_count
        self.map_width = map_width
        self.map_height = map_height
        self.minX = -7.0
        self.maxX = self.map_width - 7.0
        self.minY = -2.0;
        self.maxY = self.map_height - 2.0;
        print ("minX=%f, maxX=%f, minY=%f, maxY=%f" %(self.minX, self.maxX, self.minY, self.maxY))
        self.particles = Particle.create_random(particles_count, self.minX, self.maxX, self.minY, self.maxY, sensors_count)
        
        self.ObsMap = ObstacleMap(map_data, -7.0, -2.0, map_scale)
          
        self.ang_xy = 0
        self.vx = 0
        self.vy = 0
        self.center = Point(0, 0)

    def update_by_sensor(self, sensor_index, sensor_angle, distance,
                         sensor_max_range=1):
        """Update particles's weights and positions according to sensorses data
        and time interval
        """

        # calculate new particles weights
        output = True
        for p in self.particles:
            # TODO: add check that place is free
            p_d = self.ObsMap.calc_range(p.x, p.y, sensor_angle + p.h,
                                         sensor_max_range)
            g = w_gauss(distance, p_d);
            new_weight = w_gauss(distance, p_d) / self.sensors_count
            p.w = sum(p.weights) - p.weights[sensor_index] + new_weight
            p.weights[sensor_index] = new_weight
            #print ("X=%f Y=%f Angle=%f DistMap=%f Distance=%f Gauss=%f Weight=%f" % (p.x, p.y, sensor_angle + p.h,p_d,distance,g, p.w))
            if output:
                output = False
                # print("angle: %f; dist: %f; p_d: %f; p.x: %f; p.y: %f;" %
                #       (sensor_angle, distance, p_d, p.x, p.y))

        # ---------- Shuffle particles ----------
        new_particles = []

        # Normalise weights
        nu = sum(p.w for p in self.particles)
        if nu:
            for p in self.particles:
                p.w = p.w / nu

        #print("After normolaize")
        #for p in self.particles:
	#  print p                
                
        # create a weighted distribution, for fast picking
        particles = self.particles[:]
        dist = WeightedDistribution(self.particles)

        #print("Distribution")
        #for d in dist.distribution:
	#  print d      
        
        for _ in range(self.particles_count):
            p = dist.pick()
            #print ("Pick particle")
            #print p
            if p is None:  # No pick b/c all totally improbable
                new_particle = Particle.create_random(1, self.minX, self.maxX, self.minY, self.maxY, self.sensors_count)[0]
            else:
                new_particle = Particle.copy(p, noisy=True)
            new_particles.append(new_particle)

        self.particles = new_particles

        # calculate new position according to weigts
        self.center = Point()
        # Normalise weights
        nu = sum(p.w for p in self.particles)
        if nu:
            for p in self.particles:
                p.w = p.w / nu
        for p in self.particles:
            self.center.x += p.x * p.w
            self.center.y += p.y * p.w
            self.center.h += p.h * p.w
        print("x=%f, y=%f, h=%f" %
              (self.center.x, self.center.y, self.center.h))

    def particles_set_heading(self, h):
        for p in self.particles:
            p.h = h

    def particles_reset_weights(self, w=0, sensor_index=None):
        for p in self.particles:
            if sensor_index:
                p.weights[sensor_index] = w
            else:
                p.w = w
                p.weights = [w / self.sensors_count
                             for _ in range(self.sensors_count)]

    def particles_advance(self, vx, vy, dtime):
        # calculate additional angle according to vx and vy
        ang_xy = get_ang_xy(vx, vy)
        # calculate velocity
        v = math.sqrt(vx**2 + vy**2)
        # move particles according to belief of movement (this may
        # be different than the real movement)
        for p in self.particles:
            # move particle
            p.advance_by(v, dtime, ang_xy)

#!/bin/sh

#
# Initialization script for Arduino stuff
#
# Loads modules and launches proxy server application
# required for communication with Arduino module.
#

echo Enabling USB, powering up Arduino and launching AT Commands Proxy

echo Loading usbserial.ko
insmod /data/video/usbserial.ko

echo Loading ftdi_sio.ko
insmod /data/video/ftdi_sio.ko

#echo Waiting 3s for the device to be ready
#sleep 15

#echo Setting correct baud rate of 115200 for /dev/usb1
#stty -F /dev/ttyUsb0 115200

#echo Launching AT Command Proxy server
#/data/video/ard_proxy_arm

//

#ifndef __CTS_
#define __CTS_

#include <cmath>


// Sizes
#define STATE_SIZE 3 //state: x,y,theta
#define INPUT_SIZE 2 //input: v*deltat, omega*deltat
#define MEAS_SIZE 3  //USmeasurment: distance_to_wall

#define NUM_SAMPLES 500 // Default Number of Samples
#define RESAMPLE_PERIOD 1.0 // Default Resample Period
#define RESAMPLE_THRESHOLD (NUM_SAMPLES/4.0) // Threshold for Dynamic Resampling

// Prior:
// Initial estimate of position and orientation
#define PRIOR_MU_X 0.0
#define PRIOR_MU_Y 0.0
#define PRIOR_MU_THETA 0.0 //M_PI/2	//M_PI/4
// Initial covariances of position and orientation
#define PRIOR_COV_X pow(0.01,2)
#define PRIOR_COV_Y pow(0.01,2)
#define PRIOR_COV_THETA pow(M_PI/1000,2)

// System Noise
#define MU_SYSTEM_NOISE_X 0.0 
#define MU_SYSTEM_NOISE_Y 0.0 
#define MU_SYSTEM_NOISE_THETA 0.0
#define SIGMA_SYSTEM_NOISE_X pow(0.01,2)
#define SIGMA_SYSTEM_NOISE_Y pow(0.01,2)
#define SIGMA_SYSTEM_NOISE_THETA pow(2*M_PI/1800,2)

// Measurement noise
#define SIGMA_MEAS_NOISE pow(0.1,2) //0.05
#define MU_MEAS_NOISE 0.0

#endif //__CTS
